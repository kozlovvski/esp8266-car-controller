#include "ResetButtonHandler.h"

// public

void ResetButtonHandlerClass::init()
{
  pinMode(0, INPUT_PULLUP);
}

/** 
 * * If reset button has been pressed for more than a second,
 * * restart the device
 * ! this also resets the saved WiFi configuration
 * **/

void ResetButtonHandlerClass::loop(EEPROMHandlerClass &EEPROMHandler)
{
  if (millis() - lastMeasureMillis > MEASURE_INTERVAL)
  {
    if (digitalRead(0) == 0)
    {
      EEPROMHandler.resetEEPROM(true);
    }
    else
    {
      lastMeasureMillis = millis();
    }
  }
}