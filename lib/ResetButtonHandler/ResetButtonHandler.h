// EepromDataHandler.h

#ifndef __RESET_BUTTON_HANDLER
#define __RESET_BUTTON_HANDLER

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#include <ResetButtonHandler.h>
#include <../EEPROMHandler/EEPROMHandler.h>

class ResetButtonHandlerClass
{
private:
  const unsigned long MEASURE_INTERVAL = 1000;
  unsigned long lastMeasureMillis;

protected:
public:
  void init();
  void loop(EEPROMHandlerClass &EEPROMHandler);
};

extern ResetButtonHandlerClass ResetButtonHandler;

#endif