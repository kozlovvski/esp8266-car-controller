// EepromDataHandler.h

#ifndef _EEPROM_HANDLER
#define _EEPROM_HANDLER

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#include <EEPROM.h>

class EEPROMHandlerClass
{
private:
  String _ssid;
  String _password;

  void writeToEEPROM(String value, int start, int end);
  void readEEPROMToClass();

protected:
public:
  String getSsid();
  String getPassword();

  void resetEEPROM(bool restart);
  void writeInputToEEPROM(String ssid, String password);
  void init();
};

extern EEPROMHandlerClass EEPROMHandler;

#endif