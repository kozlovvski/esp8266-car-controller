#include "EEPROMHandler.h"

const int EEPROMsize = 512;

/* EEPROM map:

0 - 63: reserved
64 - 127: ssid
128 - 191: password

*/

// private

void EEPROMHandlerClass::writeToEEPROM(String input, int start, int end)
{
  for (int i = 0; i < end; ++i)
  {
    if (i < input.length())
    {
      EEPROM.write(start + i, input[i]);
    }
    else
    {
      EEPROM.write(start + i, 255);
    }
  }
}

void EEPROMHandlerClass::readEEPROMToClass()
{
  EEPROM.begin(EEPROMsize);
  Serial.println("Starting EEPROM read...");

  String ssid;
  for (int i = 64; i < 128; ++i)
  {
    const uint8_t readValue = EEPROM.read(i);
    if (readValue == 255)
    {
      break;
    }
    else
    {
      ssid += char(readValue);
    }
  }
  if (ssid.length() > 0)
  {
    _ssid = ssid;
  }
  Serial.println(ssid);

  String password;
  for (int i = 128; i < 192; ++i)
  {
    const uint8_t readValue = EEPROM.read(i);
    if (readValue == 255)
    {
      break;
    }
    else
    {
      password += char(readValue);
    }
  }
  if (password.length() > 0)
  {
    _password = password;
  }

  EEPROM.end();
  Serial.println("Finished EEPROM read!");
}

// public

String EEPROMHandlerClass::getSsid()
{
  return _ssid;
}

String EEPROMHandlerClass::getPassword()
{
  return _password;
}

void EEPROMHandlerClass::resetEEPROM(bool restart)
{
  EEPROM.begin(EEPROMsize);

  for (int i = 0; i < EEPROMsize; ++i)
  {
    EEPROM.write(i, 255);
  }

  EEPROM.end();
  Serial.println("EEPROM clean");

  if (restart)
  {
    ESP.restart();
  }
}

void EEPROMHandlerClass::writeInputToEEPROM(String ssid, String password)
{
  EEPROM.begin(EEPROMsize);
  Serial.println("Starting EEPROM write...");

  writeToEEPROM(ssid, 64, 128);
  writeToEEPROM(password, 128, 192);

  EEPROM.end();
  Serial.println("Finished EEPROM write!");
}

void EEPROMHandlerClass::init()
{
  _ssid = "";
  _password = "";

  readEEPROMToClass();
}
