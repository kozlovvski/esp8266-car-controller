#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include "../lib/EEPROMHandler/EEPROMHandler.h"
#include "../lib/WiFiHandler/WiFiHandler.h"
#include "../lib/ResetButtonHandler/ResetButtonHandler.h"

EEPROMHandlerClass EEPROMHandler;
WiFiHandlerClass WiFiHandler;
ResetButtonHandlerClass ResetButtonHandler;

void setup()
{
	Serial.begin(9600);

	EEPROMHandler.init();
	WiFiHandler.init(EEPROMHandler.getSsid(), EEPROMHandler.getPassword());
	ResetButtonHandler.init();
}

void loop()
{
	ResetButtonHandler.loop(EEPROMHandler);
}